# ROCM-gem5

This repo presents the modified ROCM software stack for gem5 GCN3 APU simulator. 
This repo provides a way for people to run HIP apps on the gcn3-gem5. 
Welcome people to use this repo to do any testing for gcn3-gem5. 

System Prerequisite 

1. Ubuntu 16.04 with gcc 5.4.0

2. Install packages for ROCM software and gem5

	sudo apt-get libpci-dev libelf-dev libunwind-dev python-dev libgoogle-perftools-dev scons libpng-dev automake

3. Set up environment variable in .bashrc
	
	export ROCM_PATH=/opt/rocm
	
	export HIP_PATH=${ROCM_PATH}/hip
	
	export HCC_HOME=${ROCM_PATH}/hcc
	
	export HIP_PLATFORM=hcc
	
	export PATH=${ROCM_PATH}/bin:${ROCM_PATH}/bin:${ROCM_PATH}/hcc/bin:${ROCM_PATH}/hip/bin:${ROCM_PATH}/opencl/bin:$PATH
	
	export LD_LIBRARY_PATH=${ROCM_PATH}/libhsakmt/lib:${ROCM_PATH}/hsa/lib:${ROCM_PATH}/hcc/lib:${ROCM_PATH}/hcc/compiler/lib:${ROCM_PATH}/lib:/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH

4. Building ROCM
	
	We suggest to install all ROCM software in "/opt/rocm" path
	
	a. libhsakmt

		cd libhsakmt && mkdir build && cd build
		
		cmake -DCMAKE_INSTALL_PREFIX=/opt/rocm/libhsakmt -DCMAKE_BUILD_TYPE=Release ../
		
		make && make install
	
	b. hsa

		cd hsa/src && mkdir build && cd build
		
		cmake -DCMAKE_INSTALL_PREFIX=/opt/rocm/hsa -DCMAKE_PREFIX_PATH="/opt/rocm/libhsakmt" -DCMAKE_BUILD_TYPE=Release ../
		
		make && make isntall

		cd /opt/rocm/hsa/lib && ln -s /opt/rocm/libhsakmt/lib/libhsakmt.so libhsakmt.so.1 && ln -s /opt/rocm/hsa/lib/libhsa-runtime64.so libhsa-runtime64.so.1

	c. hcc

		git clone --recursive -b roc-1.6.0 https://github.com/RadeonOpenCompute/hcc.git

		cd hcc && mkdir build && cd build
		
		cmake -DCMAKE_INSTALL_PREFIX=/opt/rocm/hcc -DCMAKE_PREFIX_PATH="/opt/rocm/hsa;/opt/rocm/libhsakmt" -DCMAKE_BUILD_TYPE=Release ../
		
		make -j 4 && make install
	
	d. hip

		cd hip && mkdir build && cd build
		
		cmake -DHIP_PLATFORM=hcc -DCMAKE_INSTALL_PREFIX=/opt/rocm/hip -DHCC_HOME=/opt/rocm/hcc -DCMAKE_PREFIX_PATH="/opt/rocm/hsa;/opt/rocm/libhsakmt;/optrocm/hcc/rocdl/lib" -DCMAKE_BUILD_TYPE=Release -DHSA_PATH=/opt/rocm/hsa ../
		
		make -j 4 && make install


5. Build up gem5
	
	scons -j 4 ./build/GCN3_X86/gem5.opt

	change LIB path in configs/example/apu_se.py

	 env = ['LD_LIBRARY_PATH=%s' % ':'.join([

	 		"/opt/rocm/lib",

			"/opt/rocm/hcc/lib",

			"/opt/rocm/hip/lib",

			"/opt/rocm/hsa/lib",

			"/opt/rocm/libhsakmt/lib",

			"/usr/lib/x86_64-linux-gnu"

			 ]),
			            
			"HSA_ENABLE_INTERRUPT=0"]

6. Build up testing square benchmark
	
	cd benchmark/square && make

7. Run square benchmark by using gem5
	./build/GCN3_X86/gem5.opt configs/example/apu_se.py -n 2 -c benchmark/square/square.bin


